/*
Create by Learn Web Developement
Youtube channel : https://www.youtube.com/channel/UC8n8ftV94ZU_DJLOLtrpORA
*/

const cvs = document.getElementById("snake");
const ctx = cvs.getContext("2d");

// create the unit
const box = 32;

// load img

const ground = new Image();
ground.src = Flask.url_for("static", {"filename": "img/ground.png"});

const foodImg = new Image();
foodImg.src = Flask.url_for("static", {"filename": "img/food.png"});

const headImgD = new Image();
headImgD.src = Flask.url_for("static", {"filename": "img/headDown.png"});
const headImgL = new Image();
headImgL.src = Flask.url_for("static", {"filename": "img/headLeft.png"});
const headImgU = new Image();
headImgU.src = Flask.url_for("static", {"filename": "img/headUp.png"});
const headImgR = new Image();
headImgR.src = Flask.url_for("static", {"filename": "img/headRight.png"});


const body = new Image();
body.src = Flask.url_for("static", {"filename": "img/body.png"});


// load audio files

let dead = new Audio();
let eat = new Audio();
let up = new Audio();
let right = new Audio();
let left = new Audio();
let down = new Audio();

dead.src =  Flask.url_for("static", {"filename": "audio/dead.mp3"});
eat.src =  Flask.url_for("static", {"filename": "audio/eat.mp3"});
up.src = "static/audio/up.mp3";
right.src = "static/audio/right.mp3";
left.src = "static/audio/left.mp3";
down.src = "static/audio/down.mp3";

// create the snake

let snake = [];

snake[0] = {
    x: 9 * box,
    y: 10 * box
};

// create the food

let food = {
    x: Math.floor(Math.random() * 17 + 1) * box,
    y: Math.floor(Math.random() * 15 + 3) * box
};

// create the score var

let score = 0;

//control the snake

let d;

document.addEventListener("keydown", direction);

function direction(event) {
    let key = event.keyCode;
    if (key == 37 && d != "RIGHT") {
        left.play();
        d = "LEFT";
    } else if (key == 38 && d != "DOWN") {
        d = "UP";
        up.play();
    } else if (key == 39 && d != "LEFT") {
        d = "RIGHT";
        right.play();
    } else if (key == 40 && d != "UP") {
        d = "DOWN";
        down.play();
    }
}

function Down() {
    if (d != "UP") {
        d = "DOWN";
        down.play();
    }
}

function Up() {
    if (d != "DOWN") {
        d = "UP";
        up.play();
    }
}

function Left() {
    if (d != "RIGHT") {
        left.play();
    }
}

function Right() {
    if (d != "LEFT") {
        d = "RIGHT";
        right.play();
    }
}


/**
 * sends a request to the specified url from a form. this will change the window location.
 * @param {string} path the path to send the post request to
 * @param {object} params the paramiters to add to the url
 * @param {string} [method=post] the method to use on the form
 */

function post(path, params, method) {
    method = method || "post"; // Set method to post by default if not specified.

    // The rest of this code assumes you are not using a library.
    // It can be made less wordy if you use one.
    var form = document.createElement("form");
    form.setAttribute("method", method);
    form.setAttribute("action", path);

    for (var key in params) {
        if (params.hasOwnProperty(key)) {
            var hiddenField = document.createElement("input");
            hiddenField.setAttribute("type", "hidden");
            hiddenField.setAttribute("name", key);
            hiddenField.setAttribute("value", params[key]);

            form.appendChild(hiddenField);
        }
    }

    document.body.appendChild(form);
    form.submit();
}

// cheack collision function
function collision(head, array) {
    for (let i = 0; i < array.length; i++) {
        if (head.x == array[i].x && head.y == array[i].y) {
            return true;
        }
    }
    return false;
}

// draw everything to the canvas

function draw() {

    ctx.drawImage(ground, 0, 0);

    for (let i = 0; i < snake.length; i++) {
        ctx.drawImage(body, snake[i].x, snake[i].y)
        // ctx.fillStyle = "white";
        // ctx.fillRect(snake[i].x,snake[i].y,box,box);
        //
        // ctx.strokeStyle = "red";
        // ctx.strokeRect(snake[i].x,snake[i].y,box,box);
    }

    ctx.drawImage(foodImg, food.x, food.y);

    // old head position
    let snakeX = snake[0].x;
    let snakeY = snake[0].y;

    // ctx.drawImage(headImgR,snakeX,snakeY);

    // which direction
    if (d == "LEFT") {
        snakeX -= box;

        ctx.drawImage(headImgL, snakeX, snakeY);
    }

    if (d == "UP") {
        snakeY -= box;
        ctx.drawImage(headImgU, snakeX, snakeY);

    }

    if (d == "RIGHT") {
        snakeX += box;
        ctx.drawImage(headImgR, snakeX, snakeY);

    }
    if (d == "DOWN") {
        snakeY += box;
        ctx.drawImage(headImgD, snakeX, snakeY);

    }

    // if the snake eats the food
    if (snakeX == food.x && snakeY == food.y) {
        score++;
        eat.play();
        food = {
            x: Math.floor(Math.random() * 17 + 1) * box,
            y: Math.floor(Math.random() * 15 + 3) * box
        }
        // we don't remove the tail
    } else {
        // remove the tail
        snake.pop();
    }

    // add new Head

    let newHead = {
        x: snakeX,
        y: snakeY
    };

    // game over

    if (snakeX < box || snakeX > 17 * box || snakeY < 3 * box || snakeY > 17 * box || collision(newHead, snake)) {
        clearInterval(game);
        dead.play();
        // post("http://localhost:5000/SubmitResult",{"auth":auth,"secret":secret,"score":score})
        post("https://onlinesnake-snake.fandogh.cloud/SubmitResult",{"auth":auth,"secret":secret,"score":score})
    }

    snake.unshift(newHead);

    ctx.fillStyle = "white";
    ctx.font = "45px Changa one";
    ctx.fillText(score, 2 * box, 1.6 * box);
}

// call draw function every 100 ms

let game = setInterval(draw, 200);


