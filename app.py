from flask import *
from flask_jsglue import JSGlue
from flask_cors import CORS
from flask_pymongo import PyMongo
from flask_mail import Mail, Message
import string
import random
import json

app = Flask(__name__)
app.config['MONGO_URI'] = "mongodb://doctor:moein1234@ds213615.mlab.com:13615/snake"

mongo = PyMongo(app)
jsglue = JSGlue(app)
mail = Mail(app)
CORS(app)


def ApiExceptionHandler(func):
    def wrapper():
        try:
            return func()
        except Exception as e:
            return Response(response=e.__str__())

    wrapper.__name__ = func.__name__
    return wrapper


@app.route("/start", methods=["GET"])
def start():
    return render_template("start.html")


@app.route("/Register", methods=["POST"])
def register():
    try:
        body = dict(request.form)
        phone = body["phone"]
        fullname = body["fullname"]
        pwd = body["pwd"]
        if not str(phone).startswith("09") or len(phone) != 11:
            return render_template("start.html", InvalidPhone=True)
        if len(fullname) < 1:
            return render_template("start.html", InvalidName=True)
        if len(pwd) < 8:
            return render_template("start.html", ShortPwd=True)
        phone_cnt = mongo.db.user.find({"phone": phone}).count()
        if phone_cnt > 0:
            return render_template("start.html", DuplicatePhone=True)
        body["best"] = 0
        body["auth"] = ""
        res = mongo.db.user.insert(body)
        return redirect("/start")

    except Exception as e:
        return "error"


@app.route("/Login", methods=["GET", "POST"])
def login():
    try:
        body = dict(request.form)
        phone = body["phone"]
        pwd = body["pwd"]
        if not str(phone).startswith("09") or len(phone) != 11:
            return render_template("start.html", InvalidPhone=True)
        res = mongo.db.user.find({"phone": phone, "pwd": pwd}).count()
        if res > 0:
            auth = "".join(
                random.choice(string.ascii_uppercase +
                              string.digits +
                              string.ascii_lowercase)
                for _ in range(10))
            secret = str(random.randint(1, 100))
            res = mongo.db.user.update({"phone": phone}, {"$set": {"auth": auth, "secret": secret}})
            return redirect("/Game/" + auth + "/" + secret)
        else:
            return render_template(start, FailedLogin=True)

    except Exception as e:
        pass
        return "error"


@app.route("/Game/<string:auth>/<string:secret>")
def main(auth, secret):
    return render_template("game.html", auth=auth, secret=secret)

    if auth == "":
        return redirect("/start")
    res = mongo.db.user.find({"auth": auth}).count()
    if res > 0:
        return render_template("game.html", auth=auth, secret=secret)
    else:
        return redirect("/start")


@app.route("/SubmitResult", methods=["POST"])
def submit_result():
    try:
        body = dict(request.form)
        auth = body["auth"]
        secret = body["secret"]
        score = int(body["score"])

        usr = list(mongo.db.user.find({"auth": auth}))
        if len(usr) == 0:
            return "Invalid"
        else:
            usr = usr[0]
            if secret == usr["secret"]:
                if usr["best"] < score:
                    res = mongo.db.user.find({"auth": auth}, {"$set": {"best": score}})
                    return redirect("/ScoreBoard")
                return redirect("/ScoreBoard")
            else:
                return "Invalid "

    except Exception as e:
        return "Error"


@app.route("/ScoreBoard")
def score_board():
    res = mongo.db.user.find({}, {"best": 1, "fullname": 1, "_id": 0}).sort([("best", -1), ])
    rows = []
    i = 1
    for usr in list(res):
        temp = []
        temp.append(i)
        i += 1
        temp.append(usr["fullname"])
        temp.append(usr["best"])
        rows.append(temp)
    d = dict()
    d["data"] = rows
    return render_template("score_board.html", result=json.dumps(d))


@app.route("/", methods=["GET"])
def index():
    return redirect("start")


if __name__ == '__main__':
    app.run(host="0.0.0.0", debug=True)
